package com.uniajc.pqrspro.pqrspro_project.service.impl;

import com.uniajc.pqrspro.pqrspro_project.exception.PqrsException;
import com.uniajc.pqrspro.pqrspro_project.modelo.Usuario;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.UsuarioDto;
import com.uniajc.pqrspro.pqrspro_project.service.UsuarioService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class UsuarioServiceImplTest {

    @Autowired
    UsuarioService usuarioService;

    //Inicio test servicio loguear
    @DisplayName(value = "test Auth -> Cuando se pasa una contraseña erronea.")
    @Test
    void testPassword() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsername("srincon");
        usuarioDto.setPassword("1111111");
        Usuario resultReal = usuarioService.loguear(usuarioDto);
        assertThat(resultReal).isNull();
    }

    @DisplayName(value = "test Auth -> Cuando se pasa un user erroneo.")
    @Test
    void testUser() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsername("perez");
        usuarioDto.setPassword("1111111");
        Usuario resultReal = usuarioService.loguear(usuarioDto);
        assertThat(resultReal).isNull();
    }

    @DisplayName(value = "test Auth -> Cuando no se envia una contraseña.")
    @Test
    void testNoPassword() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsername("srincon");
        Usuario resultReal = usuarioService.loguear(usuarioDto);
        assertThat(resultReal).isNull();
    }

    @DisplayName(value = "test Auth -> Cuando la contraseña se va null.")
    @Test
    void testNullPassword() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsername("srincon");
        usuarioDto.setPassword("");
        Usuario resultReal = usuarioService.loguear(usuarioDto);
        assertThat(resultReal).isNull();
    }

    @DisplayName(value = "test Auth -> Cuando el user se va null.")
    @Test
    void testNullUser() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsername("");
        usuarioDto.setPassword("11111");
        Usuario resultReal = usuarioService.loguear(usuarioDto);
        assertThat(resultReal).isNull();
    }

    @DisplayName(value = "test Auth -> Cuando el user no se envia.")
    @Test
    void testNoUser() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setPassword("11111");
        Usuario resultReal = usuarioService.loguear(usuarioDto);
        assertThat(resultReal).isNull();
    }//Fin test servicio loguear

    //Inicio test servidio consultarUsuarioById
    @DisplayName(value = "test Usuario -> Consultar un usuario que no está registrado.")
    @Test
    void testUsuarioById() {
        Long id = 10000000L;
        Usuario resultReal = usuarioService.consultarUsuarioById(id);
        assertThat(resultReal).isNull();
    }

    //Inicio test servidio consultarUsuarios
    @DisplayName(value = "test Usuario -> que la respuesta de la consulta trae un listado.")
    @Test
    void testConsultarUsuarios() {
        List<Usuario> resultReal = usuarioService.consultarUsuarios();
        assertThat(resultReal.size()).isPositive();
    }

    //Inicio test servidio registrar

    @DisplayName(value = "test Auth -> Cuando se crea un usuario de forma correcta.")
    @Test
    void testForm() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(10L);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido("Cuellar2");
        usuarioDto.setCorreo("carlos2@gmail.com");
        usuarioDto.setUsername("carlos2");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        String respuestaEsperada = "Registro Exitoso";
        String respuestaOptenida = null;

            respuestaOptenida = usuarioService.registrar(usuarioDto);

        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia null el formulario .")
    @Test
    void testNullForm() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(null);
        usuarioDto.setNombre(null);
        usuarioDto.setApellido(null);
        usuarioDto.setCorreo(null);
        usuarioDto.setUsername(null);
        usuarioDto.setPassword(null);
        usuarioDto.setRol(null);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia null el id .")
    @Test
    void testNullId() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(null);
        usuarioDto.setNombre("4");
        usuarioDto.setApellido("Cuellar");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia null en el nombre.")
    @Test
    void testNullUsuario() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre(null);
        usuarioDto.setApellido("Cuellar");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia null en el Apellido.")
    @Test
    void testNullLastName() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("cristhian");
        usuarioDto.setApellido(null);
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia null en el correo.")
    @Test
    void testNullEmail() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("cristhian");
        usuarioDto.setApellido("/rincon");
        usuarioDto.setCorreo(null);
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia null en el usuario.")
    @Test
    void testNullUsers() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("cristhian");
        usuarioDto.setApellido("rincon");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername(null);
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia null en el password.")
    @Test
    void testNulPassword() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("carlos");
        usuarioDto.setApellido("rincon");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword(null);
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia null en el rol.")
    @Test
    void testNullRol() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("cristhian");
        usuarioDto.setApellido("rincon");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(null);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia vacio el formulario.")
    @Test
    void testEmptyForm() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(null);
        usuarioDto.setNombre(" ");
        usuarioDto.setApellido(" ");
        usuarioDto.setCorreo(" ");
        usuarioDto.setUsername(" ");
        usuarioDto.setPassword(" ");
        usuarioDto.setRol(null);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia vacio el nombre.")
    @Test
    void testEmptyName() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre(" ");
        usuarioDto.setApellido("rincon");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia vacio el apellido.")
    @Test
    void testEmptyLastName() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido(" ");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia vacio el email.")
    @Test
    void testEmptyEmail() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido("cardoso");
        usuarioDto.setCorreo(" ");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia vacio el usuario.")
    @Test
    void testEmptyUser() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido("cardoso");
        usuarioDto.setCorreo("pruebas@gmail.com");
        usuarioDto.setUsername(" ");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia vacio el password.")
    @Test
    void testEmptyPassword() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido("cardoso");
        usuarioDto.setCorreo("pruebas@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword(" ");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envian caracteres en el formulario.")
    @Test
    void testCharacterForm() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long)4);
        usuarioDto.setNombre("/");
        usuarioDto.setApellido("<>");
        usuarioDto.setCorreo("<@/>");
        usuarioDto.setUsername("/*");
        usuarioDto.setPassword("/*<");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia un caracter en el nombre.")
    @Test
    void testCharacterName() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre("/");
        usuarioDto.setApellido("rincon");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia un caracter en el apellido.")
    @Test
    void testCharacterLastName() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 5);
        usuarioDto.setNombre("laura");
        usuarioDto.setApellido("/*<@>");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }


    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia un caracter en el usuario.")
    @Test
    void testCharacterUser() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 5);
        usuarioDto.setNombre("laura");
        usuarioDto.setApellido("cuellar");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("/*<@>");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario y se envia un caracter en el usuario.")
    @Test
    void testCharacterPassword() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 5);
        usuarioDto.setNombre("laura");
        usuarioDto.setApellido("cuellar");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("/*><");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.registrar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }
    //Fin test servicio registrar

    //Inicio test servicio actualizar

    @DisplayName(value = "test Auth -> Cuando se actualiza un usuario de forma correcta.")
    @Test
    void testUpdateForm() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(10L);
        usuarioDto.setNombre("carlos");
        usuarioDto.setApellido("Cuellar");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("carlos1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        String respuestaEsperada = "Registro Exitoso";
        String respuestaOptenida = null;
        respuestaOptenida = usuarioService.actualizar(usuarioDto);
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza un usuario y se envia null en el id.")
    @Test
    void testUpdateNullId() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(null);
        usuarioDto.setNombre("laura");
        usuarioDto.setApellido("cuellar");
        usuarioDto.setCorreo("carlos@gmail.com");
        usuarioDto.setUsername("laura1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.actualizar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza un usuario y se envia null en el formulario.")
    @Test
    void testUpdateNullForm() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(null);
        usuarioDto.setNombre(null);
        usuarioDto.setApellido(null);
        usuarioDto.setCorreo(null);
        usuarioDto.setUsername(null);
        usuarioDto.setPassword(null);
        usuarioDto.setRol(null);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.actualizar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza un usuario y se envia null en el name.")
    @Test
    void testUpdateNullName() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long)4);
        usuarioDto.setNombre(null);
        usuarioDto.setApellido("cuellar");
        usuarioDto.setCorreo("prueba@gmail.com");
        usuarioDto.setUsername("laura1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.actualizar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = "test Auth -> Cuando se actualiza un usuario y se envia null en el last name.")
    @Test
    void testUpdateNullLastName() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long)4);
        usuarioDto.setNombre(null);
        usuarioDto.setApellido("cuellar");
        usuarioDto.setCorreo("prueba@gmail.com");
        usuarioDto.setUsername("laura1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.actualizar(usuarioDto));
        String respuestaEsperada = "Los campos no cumplen las condiciones";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = " test Auth -> cuando se intenta eliminar un id que no existe")
    @Test
    void testDeleteNotId(){
        Exception exception = assertThrows(PqrsException.class,()->usuarioService.eliminar(1000000L));
        String respuestaEsperada = "Ocurrio un error al eliminar, el id no existe en la base de datos";
        String respuestaOptenida = exception.getMessage();
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = " test Auth -> cuando se elimina un id")
    @Test
    void testDeleteId(){
        String respuestaEsperada = "Eliminado con Exito";
        String respuestaOptenida = usuarioService.eliminar(10L);
        assertTrue(respuestaOptenida.contains(respuestaEsperada));
    }

    @DisplayName(value = " test Auth -> cambio de clave")
    @Test
    void testCambioPasswor() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long)4);
        usuarioDto.setNombre(null);
        usuarioDto.setApellido("cuellar");
        usuarioDto.setCorreo("prueba@gmail.com");
        usuarioDto.setUsername("laura1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Usuario respuesta = usuarioService.cambiarClave(usuarioDto);
        assertThat(respuesta).isNull();
    }

    @DisplayName(value = " test Auth -> cambio de clave")
    @Test
    void testGoCambioPasswor() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long)4);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido("cuellar");
        usuarioDto.setCorreo("prueba@gmail.com");
        usuarioDto.setUsername("laura1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        Usuario respuesta = usuarioService.cambiarClave(usuarioDto);
        assertThat(respuesta).isNull();
    }



}