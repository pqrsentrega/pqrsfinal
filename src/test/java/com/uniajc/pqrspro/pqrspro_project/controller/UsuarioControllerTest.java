package com.uniajc.pqrspro.pqrspro_project.controller;

import com.uniajc.pqrspro.pqrspro_project.modelo.dto.UsuarioDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.security.NoSuchAlgorithmException;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UsuarioControllerTest {

    @Autowired
    UsuarioController usuarioController;

    @DisplayName(value = "test Usuario -> que la respuesta de la consulta trae un listado.")
    @Test
    void testConsultarUsuarios() {
        ResponseEntity resultReal = usuarioController.consultarUsuarios();
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario de forma correcta.")
    @Test
    void testForm() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(10L);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido("Cuellar2");
        usuarioDto.setCorreo("carlos2@gmail.com");
        usuarioDto.setUsername("jose1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);

        ResponseEntity respuestaOptenida = usuarioController.registrar(usuarioDto);

        assertThat(respuestaOptenida).isNotNull();
    }

    @DisplayName(value = "test Auth -> Cuando se crea un usuario de forma correcta.")
    @Test
    void testUpdateForm() {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(10L);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido("Cuellar2");
        usuarioDto.setCorreo("carlos2@gmail.com");
        usuarioDto.setUsername("jose1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);

        ResponseEntity respuestaOptenida = usuarioController.actualizar(usuarioDto);

        assertThat(respuestaOptenida).isNotNull();
    }

    @DisplayName(value = "test Usuario -> Consultar un usuario que no está registrado.")
    @Test
    void testUsuarioById() {
        Long id = 1L;
        ResponseEntity resultReal = usuarioController.consultarUsuarioById(id);
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = " test Auth -> cuando se elimina un id")
    @Test
    void testDeleteId() {
        ResponseEntity respuestaOptenida = usuarioController.eliminar(10L);
        assertThat(respuestaOptenida).isNotNull();
    }

    @DisplayName(value = "test Auth -> Cuando se pasa una contraseña erronea.")
    @Test
    void testPassword() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsername("srincon");
        usuarioDto.setPassword("1111111");
        ResponseEntity resultReal = usuarioController.logear(usuarioDto);
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Auth -> Cuando se pasa una contraseña erronea.")
    @Test
    void testGoPassword() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsername("srincon");
        usuarioDto.setPassword("80367ed3e3");
        ResponseEntity resultReal = usuarioController.logear(usuarioDto);
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = " test Auth -> cambio de clave")
    @Test
    void testCambioPasswor() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId((long) 4);
        usuarioDto.setNombre(null);
        usuarioDto.setApellido("cuellar");
        usuarioDto.setCorreo("prueba@gmail.com");
        usuarioDto.setUsername("laura1");
        usuarioDto.setPassword("112233");
        usuarioDto.setRol(1);
        ResponseEntity respuesta = usuarioController.cambiarclave(usuarioDto);
        assertThat(respuesta).isNotNull();
    }

    @DisplayName(value = " test Auth -> cambio de clave")
    @Test
    void testGoCambioPasswor() throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setId(1L);
        usuarioDto.setNombre("jose");
        usuarioDto.setApellido("Cuellar2");
        usuarioDto.setCorreo("arinxon@yomail.com");
        usuarioDto.setUsername("arincon");
        usuarioDto.setPassword("01234569");
        usuarioDto.setRol(1);
        ResponseEntity respuesta = usuarioController.cambiarclave(usuarioDto);
        assertThat(respuesta).isNotNull();
    }
}