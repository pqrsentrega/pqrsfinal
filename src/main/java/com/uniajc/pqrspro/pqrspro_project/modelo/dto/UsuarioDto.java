package com.uniajc.pqrspro.pqrspro_project.modelo.dto;

import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDto implements Serializable {

    private Long id;
    private String nombre;
    private String apellido;
    private String correo;
    private String username;
    private String password;
    private Integer rol;


    public boolean validarDto() {

        if (null == id) {
            return false;
        }
        return validarCampos();
    }

    private boolean validarCampos() {

        if (StringUtils.isBlank(nombre)) {
            return false;
        }
        if (StringUtils.isBlank(apellido)) {
            return false;
        }
        if (StringUtils.isBlank(correo)) {
            return false;
        }
        if (StringUtils.isBlank(username)) {
            return false;
        }
        if (StringUtils.isBlank(password)) {
            return false;
        }
        if (null == rol) {
            return false;
        }

        return validarCaracteres();

    }

    private boolean validarCaracteres() {

        String caracteresEspeciales = "|@&$</>";

        if (StringUtils.containsAny(nombre, caracteresEspeciales)) {
            return false;
        }
        if (StringUtils.containsAny(apellido, caracteresEspeciales)) {
            return false;
        }
        if (StringUtils.containsAny(username, caracteresEspeciales)) {
            return false;
        }
        if (StringUtils.containsAny(password, caracteresEspeciales)) {
            return false;
        }
        return true;
    }


}
