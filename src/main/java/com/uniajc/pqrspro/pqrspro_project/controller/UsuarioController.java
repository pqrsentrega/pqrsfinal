package com.uniajc.pqrspro.pqrspro_project.controller;

import com.uniajc.pqrspro.pqrspro_project.modelo.Usuario;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.GenericDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.UsuarioDto;
import com.uniajc.pqrspro.pqrspro_project.security.JwtToken;
import com.uniajc.pqrspro.pqrspro_project.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/user")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;


    @PostMapping("/logear")
   
    public ResponseEntity<GenericDto> logear(@RequestBody UsuarioDto usuarioDtox) throws NoSuchAlgorithmException {
        UsuarioDto usuarioDto = new UsuarioDto();
        usuarioDto.setUsername(usuarioDtox.getUsername());
        usuarioDto.setPassword(usuarioDtox.getPassword());
        JwtToken objToken = new JwtToken();
        String token = objToken.getJWTToken(usuarioDtox.getUsername());
        if (this.usuarioService.loguear(usuarioDto)!=null) {
            return ResponseEntity.ok().body(GenericDto.sucess((token)));
        } else {
            return ResponseEntity.ok().body(GenericDto.sucess(("error")));
        }

    }

    @PostMapping("/registrar")
   
    public ResponseEntity<GenericDto> registrar(@RequestBody UsuarioDto usuarioDto){
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.registrar(usuarioDto)));
    }

    @PutMapping("/actualizar")
   
    public ResponseEntity<GenericDto> actualizar(@RequestBody UsuarioDto usuarioDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.actualizar(usuarioDto)));
    }


    @DeleteMapping("/eliminar")
   
    public ResponseEntity<GenericDto> eliminar(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.eliminar(id)));
    }

   
    @PostMapping(path = "/cambiarclave")
    public ResponseEntity<GenericDto> cambiarclave(@RequestBody UsuarioDto usuarioDto) throws NoSuchAlgorithmException {
        Usuario usuarioCambiarClave = this.usuarioService.cambiarClave(usuarioDto);
        if (usuarioCambiarClave != null) {
            return ResponseEntity.ok().body(GenericDto.sucess(usuarioCambiarClave));
        } else {
            return ResponseEntity.ok().body(GenericDto.sucess("Los datos no coicide con el usuario ingresado"));
        }

    }

    @GetMapping("/consultarUsuarioById")
    public ResponseEntity<GenericDto> consultarUsuarioById(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.consultarUsuarioById(id)));
    }

    @GetMapping("/consultarUsuarios")
    public ResponseEntity<GenericDto> consultarUsuarios() {
        return ResponseEntity.ok().body(GenericDto.sucess(this.usuarioService.consultarUsuarios()));
    }


}
