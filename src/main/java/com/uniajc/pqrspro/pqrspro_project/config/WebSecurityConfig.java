package com.uniajc.pqrspro.pqrspro_project.config;

import com.uniajc.pqrspro.pqrspro_project.security.JWTAuthorizationFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    public static final String AUTH_URL = "/user/**";
    public static final String SWAGGER_URL = "/swagger-ui.html/**";
    public static final String SWAGGER_URL1 = "/swagger/**";
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(AUTH_URL).permitAll()
                .antMatchers(SWAGGER_URL).permitAll()
                .antMatchers(SWAGGER_URL1).permitAll()
                .anyRequest().authenticated();
    }
}
