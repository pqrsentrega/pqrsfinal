package com.uniajc.pqrspro.pqrspro_project.service.impl;

import com.uniajc.pqrspro.pqrspro_project.exception.PqrsException;
import com.uniajc.pqrspro.pqrspro_project.modelo.Usuario;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.UsuarioDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.transformacion.UsuarioTransformacion;
import com.uniajc.pqrspro.pqrspro_project.repository.UsuarioRepository;
import com.uniajc.pqrspro.pqrspro_project.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;


@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioTransformacion usuarioTransformacion;

    @Override
    public Usuario loguear(UsuarioDto usuarioDto) {
        return this.usuarioRepository.findByUsernameAndPassword(usuarioDto.getUsername(), usuarioDto.getPassword()).orElse(null);
    }

    @Override
    public Usuario cambiarClave(UsuarioDto usuarioDto) throws NoSuchAlgorithmException {
        Usuario respuesta = this.usuarioRepository.findByUsernameAndCorreo(usuarioDto.getUsername(), usuarioDto.getCorreo()).orElse(null);
        if (respuesta != null) {
            respuesta.generarPassword();
            usuarioDto.setPassword(respuesta.getPassword());
            usuarioDto.setNombre(respuesta.getNombre());
            usuarioDto.setId(respuesta.getId());
            usuarioDto.setApellido(respuesta.getApellido());
            usuarioDto.setRol(respuesta.getRol());
            actualizar(usuarioDto);
            return respuesta;
        } else {
            return null;
        }
    }

    @Override
    public String registrar(UsuarioDto usuarioDtoSave) {

        if (usuarioDtoSave.validarDto()) {
            Usuario dtoUsuarioSave = usuarioTransformacion.usuarioDtoToUsuario(usuarioDtoSave);
            usuarioRepository.save(dtoUsuarioSave);
            return "Registro Exitoso";
        }
        throw new PqrsException("Los campos no cumplen las condiciones");
    }

    @Override
    public String actualizar(UsuarioDto usuarioDtoUpdate) {
        if (usuarioDtoUpdate.validarDto()) {
            Usuario dtoUsuarioUpdate = usuarioTransformacion.usuarioDtoToUsuario(usuarioDtoUpdate);
            usuarioRepository.save(dtoUsuarioUpdate);
            return "Registro Exitoso";
        }
        throw new PqrsException("Los campos no cumplen las condiciones");
    }

    @Override
    public String eliminar(Long id) {
        if(usuarioRepository.existsById(id)){
            usuarioRepository.deleteById(id);
            return "Eliminado con Exito";
        }
        throw new PqrsException("Ocurrio un error al eliminar, el id no existe en la base de datos");
    }

    @Override
    public Usuario consultarUsuarioById(Long id) {
        return usuarioRepository.findById(id).orElse(null);
    }

    @Override
    public List<Usuario> consultarUsuarios() {
        return (List<Usuario>) usuarioRepository.findAll();
    }
}
